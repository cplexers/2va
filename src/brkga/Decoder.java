package brkga;

import java.util.ArrayList;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import po.BaseStation;
import po.PO;

public class Decoder {
    
    int K;
    Geometry city;

    public Decoder(int K, Geometry city) {
        this.K = K;
        this.city = city;
    }
    
    public int n() {
        return K;
    }
    
    public double ft(Geometry city, Geometry tot) {        
        if (tot == null)
            return 0;
        return tot.intersection(city).getArea() / city.getArea();
    }
    
    public double fe() {
        return ((double)K - (double)n()) / (double)K;
    }
    
    public double fitness(Geometry city, Geometry tot, double wt, double we) {
        return wt * ft(city, tot) + we * fe();
    }
    
    public double decode(ArrayList<Double> chromosome) {
        double fitness = 0.0;
        
        List<BaseStation> floatStations = this.getFloatStations(chromosome);

        int i = 0;
        
        Geometry tot = null;
        for (BaseStation floatStation : floatStations) {
            if (floatStation.active) {
                Coordinate coord = new Coordinate(floatStation.x, floatStation.y);
                Geometry newStation = PO.createCircle(PO.geoFactory, coord, floatStation.radius);
                if (tot == null)
                    tot = new GeometryFactory().createGeometry(newStation);
                else
                    tot = tot.union(newStation);
                i++;
            }
        }
        fitness = fitness(city, tot, 1.0f, 0.0f);
        return fitness;
    }
    
    public List<BaseStation> getFloatStations(ArrayList<Double> chromosome) {
        List<BaseStation> floatStations = new ArrayList<>();
        
        for (int i = 0; i < chromosome.size() - 1; i += 4) {
            floatStations.add(new BaseStation(chromosome.get(i) < 0.5, chromosome.get(i + 1) * PO.xMax,
                    chromosome.get(i + 2) * PO.yMax, chromosome.get(i + 3) * PO.maxRadius));
        }
        
        return floatStations;
    }
}
