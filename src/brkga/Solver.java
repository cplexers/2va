package brkga;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Coordinate;
import po.BaseStation;
import po.PO;


public class Solver {
    private static int n;        // size of chromosomes = no of transfers
    private static final int p = 100;    // size of population
    private static final double pe = 0.2;        // fraction of population to be the elite-set
    private static final double pm = 0.3;        // fraction of population to be replaced by mutants
    private static final double rho = 0.7;    // probability that offspring inherit an allele from elite parent
    private static final int I = 2;        // number of independent populations

    public static List<Geometry> baseStations = new ArrayList<>();

    public static void solve(Geometry city, int K, int M, int maxGen, JFrame frame,
                             long[][] memSeconds, double[][] memCoverages, int round, Instant start) throws IOException {

        Decoder decoder = new Decoder(K, city);
        Brkga brkga = new Brkga(K * 4, M, pe, pm, rho, decoder, I);
        int generation = 0;        // current generation
        final int X_INTVL = 2;    // exchange best individuals at every X_INTVL generations
        final int X_NUMBER = 10;    // exchange top 10 best
        final int MAX_GENS = maxGen;    // run for MAX_GENS gens
        int actualGen = 1;

        double bestCoverage = 0;
        do {
            brkga.evolve(1);    // evolve the population for one generation

            if ((++generation) % X_INTVL == 0) {
                brkga.exchangeElite(X_NUMBER);    // exchange top individuals
            }
            double currCoverage = brkga.getBestFitness();
            if (generation == 1 || (generation % 25 == 0 && currCoverage > bestCoverage)) {
                List<BaseStation> floatStations = decoder.getFloatStations(brkga.getBestChromosome());
                baseStations.clear();
                for (BaseStation station : floatStations) {
                    if (station.active) {
                        baseStations.add(PO.createCircle(PO.geoFactory, new Coordinate(station.x, station.y), station.radius));
                    }
                }
                bestCoverage = currCoverage;
                frame.getContentPane().repaint();

                try {
                    PO.frameToImage(frame, "BRKGA", actualGen, currCoverage);
                } catch (Exception ignored) {
                }
            }

            PO.logCoverage(memSeconds, memCoverages, round - 1, generation - 1, bestCoverage, start);

            actualGen++;
        } while (generation < MAX_GENS);
    }
}