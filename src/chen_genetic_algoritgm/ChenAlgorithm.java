package chen_genetic_algoritgm;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import po.BaseStation;
import po.PO;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class ChenAlgorithm {
    JFrame frame;

    private double probMutate = 0.5, Pbs = 999, density = 1;

    public static ArrayList<ArrayList<BaseStation>> population = new ArrayList<>();
    public static ArrayList<Geometry> bestIndividual = new ArrayList<>();;

    public ChenAlgorithm(JFrame frame) {
        this.frame = frame;
    }

    public void start(ArrayList<BaseStation> knowledge, int M, int K) {
        Random rand = new Random();
        double bestFitness = 0;

        // initializing population
        for (int m = 0; m < M; m++) {
            Collections.shuffle(knowledge);
            ArrayList<BaseStation> individual = new ArrayList<>();
            for (int i = 0; i < K; i++) {
                individual.add(knowledge.get(i));
            }
            population.add(individual);
        }
        population.sort(Comparator.comparingDouble(this::fitness));

        for (int gen = 1; gen <= PO.maxGenerations; gen++) {

            // crossover
            ArrayList<BaseStation> mother = population.get(Math.abs(rand.nextInt()) % M);
            ArrayList<BaseStation> father = population.get(Math.abs(rand.nextInt()) % M);
            double stochasticFactor = rand.nextFloat();

            ArrayList<BaseStation> kid1 = new ArrayList<>(mother);
            for (int k = 0; k < K; k++) {
                kid1.get(k).x = stochasticFactor * mother.get(k).x + (1 - stochasticFactor) * father.get(k).x;
                kid1.get(k).y = stochasticFactor * mother.get(k).y + (1 - stochasticFactor) * father.get(k).y;
                kid1.get(k).radius = stochasticFactor * mother.get(k).radius + (1 - stochasticFactor) * father.get(k).radius;
            }

            ArrayList<BaseStation> kid2 = new ArrayList<>(father);
            for (int k = 0; k < K; k++) {
                kid2.get(k).x = (1 - stochasticFactor) * mother.get(k).x + stochasticFactor * father.get(k).x;
                kid2.get(k).y = (1 - stochasticFactor) * mother.get(k).y + stochasticFactor * father.get(k).y;
                kid2.get(k).radius = (1 - stochasticFactor) * mother.get(k).radius + stochasticFactor * father.get(k).radius;
            }

            // Mutation
            for (int m = 0; m < M; m++) {
                for (int k = 0; k < K; k++) {
                    if (rand.nextFloat() < probMutate) {
                        population.get(m).get(k).active = rand.nextFloat() < 0.5;
                        population.get(m).get(k).x = rand.nextFloat() * PO.xMax;
                        population.get(m).get(k).y = rand.nextFloat() * PO.yMax;
                        population.get(m).get(k).radius = rand.nextFloat() * PO.maxRadius;
                    }
                }
            }

            // Selection
            double sumFitness = 0;
            for (ArrayList<BaseStation> baseStations : population) {
                sumFitness += fitness(baseStations);
            }

            ArrayList<ArrayList<BaseStation>> newPopulation = new ArrayList<>();
            for (int m = 0; m < M; m++) {
                double selectFitness = rand.nextFloat() * sumFitness, cumulativeFitness = 0;
                for (int i = 0; i < population.size(); i++) {
                    double currFitness = fitness(population.get(i));
                    cumulativeFitness += currFitness;

                    if (selectFitness <= cumulativeFitness) {
                        newPopulation.add(population.get(i));
                        population.remove(i);
                        sumFitness -= currFitness;
                        break;
                    }
                }
            }

            population = newPopulation;
            population.sort(Comparator.comparingDouble(this::fitness));

            if (gen == 1 || gen % 25 == 0) {
                double currFitness = fitness(population.get(0));
                System.out.printf("[%d, %d] %f\n", gen, bestIndividual.size(), currFitness);

                if (currFitness > bestFitness) {
                    bestFitness = currFitness;
                    bestIndividual = new ArrayList<>();

                    for (BaseStation station : population.get(0)) {
                        if (station.active) {
                            bestIndividual.add(PO.createCircle(PO.geoFactory, new Coordinate(station.x, station.y), station.radius));
                        }
                    }
                    frame.getContentPane().repaint();
                    try {
                        PO.frameToImage(frame, "ChenGA", gen, currFitness);
                    } catch (Exception exception) {
                    }
                }
            }
        }
    }

    private Double fitness(ArrayList<BaseStation> baseStations) {
        double sum = 0;
        for (BaseStation baseStation : baseStations) {
            sum += baseStation.radius / density + Pbs * (baseStation.active ? 1 : 0);
        }
        return sum;
    }
}
