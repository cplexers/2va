package genetic_algorithm;

import java.util.ArrayList;
import java.util.Random;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import po.PO;
import util.Statistics;

public class Genome {
    public final int K;
    // Chromosome list
    public ArrayList<Geometry> stations;
    
    private int n = 0;
    private final double xMax, yMax, radius;
    
    public Genome(int K, double xMax, double yMax, double radius) {
        this.xMax = xMax;
        this.yMax = yMax;
        this.radius = radius;
        
        this.K = K;
        stations = new ArrayList();
    }

    // Initialize genome based on another genome
    public Genome(Genome base, double probNull, double probExistence,
                  double xMax, double yMax, double radius) {
        this(base, probNull, probExistence, xMax, yMax, radius, 
                base.getXVariance(), base.getYVariance());
    }
    
    public Genome(Genome base, double probNull, double probExistence,
                  double xMax, double yMax, double radius,
                  double xVar, double yVar) {
//        System.out.println("new genome variances: " + xVarSquared + ", " + yVarSquared);
        this(base.K, xMax, yMax, radius);
        
        base.stations.forEach( (Geometry s) -> {
            if (s == null) {
                if (Math.random() < probNull) {
                    stations.add(null);
                } else {
                    stations.add(randomStation(radius, xMax, yMax));
                    n++;
                }
            } else {
                if (Math.random() < 1 - probExistence) {
                    stations.add(null);
                } else {
                    Random r = new Random();
                    
                    double e1 = r.nextGaussian() * xVar;
                    double e2 = r.nextGaussian() * yVar;

                    Coordinate coord = new Coordinate(s.getCoordinate());
                    coord.x += e1;
                    coord.y += e2;
                    stations.add(PO.createCircle(PO.geoFactory, coord, radius));
                    n++;
                }
            }
        });
    }
    
    // Initialize genome randomly
    public Genome(int K, double probNull, double xMax, double yMax, double radius) {
        this(K, xMax, yMax, radius);
        
        for (int i = 0; i < K; i++) {
            if (Math.random() < probNull) {
                stations.add(null);
            } else {
                stations.add(randomStation(radius, xMax, yMax));
                n++;
            }
        }
    }
    
    private double getXVariance() {
        int idx = 0;
        double[] xValues = new double[n];

        for (int k = 0; k < K; k++) {
            if (stations.get(k) != null) {
                xValues[idx] = stations.get(k).getCoordinate().x;
                idx++;
            }
        }

        return Statistics.variance(xValues);
    }
    
    private double getYVariance() {
        int idx = 0;
        double[] yValues = new double[n];

        for (int k = 0; k < K; k++) {
            if (stations.get(k) != null) {
                yValues[idx] = stations.get(k).getCoordinate().y;
                idx++;
            }
        }

        return Statistics.variance(yValues);
    }
    
    private Geometry randomStation(double radius, double xMax, double yMax) {
        double x = Math.random() * xMax;
        double y = Math.random() * yMax;
        Coordinate coord = new Coordinate(x, y);
        
        return PO.createCircle(PO.geoFactory, coord, radius);
    }
    
    public Genome crossover(Genome mom, double sigmaC) {
        Genome dad = this;
        Genome child = new Genome(dad.K, dad.xMax, dad.yMax, dad.radius);
        
        for (int k = 0; k < dad.K; k++) {
            if (dad.stations.get(k) == null) {
                child.addStation(mom.stations.get(k));
            } else if (mom.stations.get(k) == null) {
                child.addStation(dad.stations.get(k));
            } else {
                Coordinate dadCoord = dad.stations.get(k).getCoordinate();
                Coordinate momCoord = mom.stations.get(k).getCoordinate();
                
                double x = (dadCoord.x + momCoord.x) / 2;
                double y = (dadCoord.y + momCoord.y) / 2;
                
                double xVar = (dadCoord.x - momCoord.x) * sigmaC / 2;
                double yVar = (dadCoord.y - momCoord.y) * sigmaC / 2;
                
                Random r = new Random();
                double s1 = r.nextGaussian() * xVar;
                double s2 = r.nextGaussian() * yVar;
                
                Coordinate coord = new Coordinate(x + s1, y + s2);
                child.addStation(PO.createCircle(PO.geoFactory, coord, radius));
            }
        }
        
        return child;
    }
    
    public Genome mutate(double probMutation, double probNull, double probExistence,
            double sigmaM) {
        if (Math.random() < probMutation) {
            return new Genome(this, probNull, probExistence, xMax, yMax, radius,
                    sigmaM, sigmaM);
        }
        return null;
    }
    
    public double ft(Geometry city) {
        Geometry coveredArea = null;
        for (int i = 0; i < K; i++) {
            if (stations.get(i) != null) {
                if (coveredArea == null)
                    coveredArea = PO.geoFactory.createGeometry(stations.get(i));
                else
                    coveredArea = coveredArea.union(stations.get(i));
            }
        }
        
        if (coveredArea == null)
            return 0;
        return coveredArea.intersection(city).getArea() / city.getArea();
    }
    
    public float fe() {
        return (K - n) / K;
    }
    
    public double fitness(Geometry city, double wt, double we) {
        double fit = wt * ft(city) + we * fe();
//        System.out.println(fit);
        return fit;
    }

    private void addStation(Geometry s) {
        stations.add(s);
        if (s != null) {
            n++;
        }
    }
}
