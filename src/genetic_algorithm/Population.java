package genetic_algorithm;

import java.util.ArrayList;
import java.util.Random;
import org.locationtech.jts.geom.Geometry;

public class Population {
    public ArrayList<Genome> individuals = new ArrayList();
    
    public static double probMutation = 0.3, probNull = 0.2, probExistence = 0.8,
            sigmaM = 1, sigmaC = 1, stationRadius = 62.5;
    private static final int children = 10;
    
    private final double wt, we;
    private final int M;
    private Geometry city;
    private Genome bestGenome;
    private double bestFitness = 0;
    
    public Population(Genome firstGenome, int M, double wt, double probNull,
                      double probExistence, double xMax, double yMax) {
        this.M = M;
        this.wt = wt;
        this.we = 1 - wt;
        
        individuals.add(firstGenome);
        for (int i = 1; i < M; i++) {
            individuals.add(new Genome(firstGenome, probNull, probExistence, xMax, yMax, stationRadius));
        }
    }
    
    public Population(int M, double wt, int K, double probNull, double xMax, double yMax) {
        this.M = M;
        this.wt = wt;
        this.we = 1 - wt;
        
        for (int i = 0; i < M; i++) {
            individuals.add(new Genome(K, probNull, xMax, yMax, stationRadius));
        }
    }
    
    public void setCity(Geometry city) {
        this.city = city;
        
        setBest();
    }
    
    public Genome selection() {
        double totalSum = 0;
        
        for (int i = 0; i < individuals.size(); i++) {
            totalSum += individuals.get(i).fitness(city, wt, we);
        }
        
        double r = Math.random() * totalSum;
        double sum = 0;
        for (int i = 0; i < individuals.size(); i++) {
            sum += individuals.get(i).fitness(city, wt, we);
            if (r < sum) {
                return individuals.get(i);
            }
        }
        
        return null;
    }
    
    public void nextGeneration() {
        Random r = new Random();
        ArrayList<Genome> offspring = new ArrayList();
        
        // Crossover
        for (int i = 0; i < children; i++) {
            int dadIdx = Math.abs(r.nextInt()) % M;
            Genome dad = individuals.get(dadIdx);
            int momIdx = Math.abs(r.nextInt()) % M;
            Genome mom = individuals.get(momIdx);
            
            Genome child = dad.crossover(mom, sigmaC);
            offspring.add(child);
            
            double childFitness = child.fitness(city, wt, we);
            if (childFitness > bestFitness) {
                bestGenome = child;
                bestFitness = childFitness;
            }
        }
        
        // Mutation
        for (int i = 0; i < offspring.size(); i++) {
            Genome mutant = offspring.get(i).mutate(probMutation, probNull,
                    probExistence, sigmaM);
            if (mutant != null) {
                offspring.set(i, mutant);
                
                double mutantFitness = mutant.fitness(city, wt, we);
                if (mutantFitness > bestFitness) {
                    bestGenome = mutant;
                    bestFitness = mutantFitness;
                }
            }
        }
        
        // Selection
        individuals.addAll(offspring);
        individuals.remove(bestGenome);
//        this.sortPopulation();
        
        ArrayList<Genome> newIndividuals = new ArrayList();
        newIndividuals.add(bestGenome);
        while(newIndividuals.size() < M) {
            Genome sel = selection();
            newIndividuals.add(sel);
            individuals.remove(sel);
        }
        
        individuals = newIndividuals;
    }

    public void setBest() {
        Genome best = individuals.get(0);
        double bestFit = best.fitness(city, wt, we);
        for (int i = 1; i < M; i++) {
            double currFitness = individuals.get(i).fitness(city, wt, we);
            if (currFitness > bestFit) {
                best = individuals.get(i);
                bestFit = currFitness;
            }
        }
        
        bestGenome = best;
        bestFitness = bestFit;
    }
    
    public Genome getBest() {
        return bestGenome;
    }

    private void sortPopulation() {
        individuals.sort((Genome a, Genome b) -> {
            double fitA = a.fitness(city, wt, we), fitB = b.fitness(city, wt, we);
            
            if (fitA == fitB)
                return 0;
            if (fitA < fitB)
                return -1;
            return 1;
        });
    }

    public Genome getWorst() {
        Genome worst = individuals.get(0);
        double worstFit = worst.fitness(city, wt, we);
        for (int i = 1; i < M; i++) {
            double currFitness = individuals.get(i).fitness(city, wt, we);
            if (currFitness < worstFit) {
                worst = individuals.get(i);
                worstFit = currFitness;
            }
        }
        
        return worst;
    }
}
