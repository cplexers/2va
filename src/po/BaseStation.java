package po;

public class BaseStation {
    public boolean active;
    public double x, y, radius;

    public BaseStation(boolean active, double x, double y, double radius) {
        this.active = active;
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
}
