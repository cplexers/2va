package po;

import brkga.Solver;
import chen_genetic_algoritgm.ChenAlgorithm;
import genetic_algorithm.Genome;
import genetic_algorithm.Population;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.locationtech.jts.awt.ShapeWriter;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.util.GeometricShapeFactory;
import util.Util;

public class PO extends JPanel {
    static boolean debug = true;

    static final String baseDir = "C:\\Users\\danie\\Desktop\\2va";
    public static final int id = 32, runTimes = 8, useCity = 3;
    static final Algorithms algorithm = Algorithms.BRKGA;
    public static final int maxGenerations = 500;

    static final int M = 25, K = 20;
    static final double probNull = 0, wt = 1;

    public static final double maxRadius = 70, xMax = 625, yMax = 625;

    public static GeometryFactory geoFactory = new GeometryFactory();
    static Random rand = new Random();
    Geometry city;
    Polygon container;
    Population population;

    public static Geometry createCircle(GeometryFactory geoFactory, Coordinate coordinate, double radius) {
        GeometricShapeFactory shape = new GeometricShapeFactory(geoFactory);
        shape.setCentre(coordinate);
        shape.setSize(2 * radius);
        shape.setNumPoints(32);
        return shape.createCircle();
    }

    public static void main(String[] args) throws IOException {
        long[][] memSeconds = new long[runTimes][maxGenerations];
        double[][] memCoverages = new double[runTimes][maxGenerations];

        PO po = null;
        JFrame frame = new JFrame();
        frame.setTitle("Base Station Placement");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        for (int round = 1; round <= runTimes; round++) {
            if (po != null) {
                frame.getContentPane().remove(po);
            }

            po = new PO();
            po.setup();
            frame.getContentPane().add(po);
            frame.setSize(631, 654);
            frame.setVisible(true);

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Instant start = Instant.now();
            if (algorithm == Algorithms.JinGeneticAlgorithm) {
                double bestCoverage = 0;
                for (int gen = 1; gen <= maxGenerations; gen++) {
                    po.population.nextGeneration();

                    double currCoverage = po.population.getBest().ft(po.city);

                    if (currCoverage > bestCoverage) {
                        bestCoverage = currCoverage;
                        frame.getContentPane().repaint();

                        try {
                            frameToImage(frame, "GA", gen, currCoverage);
                        } catch (Exception ignored) {
                        }
                    }

                    logCoverage(memSeconds, memCoverages, round - 1, gen - 1, bestCoverage, start);
                }
            } else if (algorithm == Algorithms.BRKGA) {
                Solver.solve(po.city, K, M, maxGenerations, frame, memSeconds, memCoverages, round, start);
            } else if (algorithm == Algorithms.ChenGeneticAlgorithm) {
                ChenAlgorithm ga = new ChenAlgorithm(frame);

                ArrayList<BaseStation> knowledge = new ArrayList<>();
                for (int i = 0; i < K * 2; i++) {
                    knowledge.add(new BaseStation(rand.nextFloat() < 0.5, rand.nextFloat() * xMax,
                            rand.nextFloat() * yMax, rand.nextFloat() * maxRadius));
                }

                ga.start(knowledge, M, K);
            }

            Long end = Instant.now().toEpochMilli();
            System.out.println();
            System.out.println("Finished! ID: " + id + "; started: " + sdf.format(start.toEpochMilli()) + ", finished: " + sdf.format(end));
            System.out.println("Runtime: " + sdf.format(end - start.toEpochMilli()));
        }

        for (int j = 0; j < maxGenerations; j++) {
            long sumSeconds = 0;
            double sumCoverages = 0;
            for(int i = 0; i < runTimes; i++) {
                sumSeconds += memSeconds[i][j];
                sumCoverages += memCoverages[i][j];
            }

            printOnFile(algorithm == Algorithms.JinGeneticAlgorithm ? "GA" : algorithm == Algorithms.BRKGA ? "BRKGA" : "ChenGA",
                    "average", sumSeconds / runTimes, sumCoverages / runTimes);
        }
        System.out.println("You can close now.");
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    void setup() {
        Point[] hexagons1 = {
                new Point(220, 286), new Point(220, 394),
                new Point(312, 231), new Point(312, 340), new Point(312, 448),
                new Point(406, 286), new Point(406, 394)
        };
        Point[] hexagons2 = {
                new Point(127, 124), new Point(127, 231), new Point(127, 340), new Point(127, 448),
                new Point(220, 178), new Point(220, 286), new Point(220, 394), new Point(220, 502),
                new Point(312, 124), new Point(312, 231), new Point(312, 340), new Point(312, 448),
                new Point(406, 178), new Point(406, 286), new Point(406, 394), new Point(406, 502),
                new Point(499, 124), new Point(499, 231), new Point(499, 340), new Point(499, 448)
        };
        Point[] hexagons3 = {
                new Point(127, 231), new Point(127, 340), new Point(127, 448),
                new Point(220, 178), new Point(220, 286), new Point(220, 394),
                new Point(312, 124), new Point(312, 231),
                new Point(406, 178), new Point(406, 286), new Point(406, 394),
                new Point(499, 231), new Point(499, 340), new Point(499, 448)
        };
        Point[] hexagonsCenters = useCity == 1 ? hexagons1 : useCity == 2 ? hexagons2 : hexagons3;
        ArrayList<Polygon> hexagons = new ArrayList<>();
        for (Point p : hexagonsCenters) {
            Coordinate[] cors = Util.polygonVertices(p.x, p.y, 63, 6);
            hexagons.add(geoFactory.createPolygon(cors));
        }

        city = hexagons.get(0);
        for (int i = 1; i < hexagons.size(); i++) {
            city = city.union(hexagons.get(i));
        }

        container = geoFactory.createPolygon(new Coordinate[]{new Coordinate(0, 0), new Coordinate(xMax, 0), new Coordinate(xMax, yMax), new Coordinate(0, yMax), new Coordinate(0, 0)});

//        System.out.println("container: " + container.getArea());
//        System.out.println("city: " + city.getArea());
//        System.out.println("container - city: " + container.difference(city).getArea());

        population = new Population(M, wt, K, probNull, xMax, yMax);
        population.setCity(city);
    }

    @Override
    public void paint(Graphics g) {
        ShapeWriter sw = new ShapeWriter();
        Shape linShape = sw.toShape(container.difference(city));
        ((Graphics2D) g).setPaint(new Color(0, 130, 0));
        ((Graphics2D) g).fill(linShape);

        linShape = sw.toShape(city);
        ((Graphics2D) g).setPaint(new Color(190, 190, 190));
        ((Graphics2D) g).fill(linShape);

        ((Graphics2D) g).setPaint(Color.WHITE);
        if (algorithm == Algorithms.JinGeneticAlgorithm) {
            Genome bestIndividual = population.getBest();
            bestIndividual.stations.forEach((Geometry s) -> {
                if (s != null) {
                    this.drawStation((Graphics2D) g, sw, s);
                }
            });
        } else if (algorithm == Algorithms.BRKGA) {
            Solver.baseStations.forEach((Geometry s) -> {
                if (s != null) {
                    this.drawStation((Graphics2D) g, sw, s);
                }
            });
        } else if (algorithm == Algorithms.ChenGeneticAlgorithm) {
            ChenAlgorithm.bestIndividual.forEach((Geometry s) -> {
                this.drawStation((Graphics2D) g, sw, s);
            });
        }
    }

    private void drawStation(Graphics2D g, ShapeWriter sw, Geometry s) {
        Shape circle = sw.toShape(s), center = sw.toShape(s.getCentroid());
        g.draw(circle);
        g.draw(center);
    }

    public static void frameToImage(JFrame frame, String algorithm, int actualGen, double currCoverage) throws IOException {
        if (debug)
            return;

        BufferedImage image = new BufferedImage(frame.getWidth(), frame.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = image.createGraphics();
        frame.paint(graphics2D);
        String fileName = baseDir.concat("\\prints\\" + algorithm + "\\")
                .concat(Integer.toString(PO.id)).concat("\\").concat(Integer.toString(actualGen)).concat(" - ")
                .concat(Double.toString(currCoverage)).concat(".jpeg");
        new File(fileName).mkdirs();
        ImageIO.write(image, "jpeg", new File(fileName));
    }

    private static void printOnFile(String algorithm, String fileName, long seconds, double currCoverage) throws IOException {
        String filePath = baseDir + "\\prints\\" + algorithm + "\\" + PO.id + "/#" + fileName + ".csv";
        new File(fileName).mkdirs();
        PrintStream out = new PrintStream(new FileOutputStream(filePath, true));
        out.println(seconds + "," + currCoverage);
        out.close();
    }

    public static void logCoverage(long[][]memSeconds, double[][] memCoverages, int round,
                                   int generation, double bestCoverage, Instant start) throws IOException {
        memSeconds[round][generation] = Instant.now().getEpochSecond() - start.getEpochSecond();
        memCoverages[round][generation] = bestCoverage;
        printOnFile(algorithm == Algorithms.JinGeneticAlgorithm ? "GA" : algorithm == Algorithms.BRKGA ? "BRKGA" : "ChenGA",
                Integer.toString(round + 1), memSeconds[round][generation], bestCoverage);
        System.out.printf("[%d, %d] best coverage: %f\n", round + 1, generation + 1, bestCoverage);
    }
}
