/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import org.locationtech.jts.geom.Coordinate;

/**
 *
 * @author Lucas Amorim
 */
public class Util {
    
    final static double TWO_PI = 2 * 3.14159265359;
    
    public static Coordinate[] polygonVertices(int x, int y, double rad, int npoints) {
        ArrayList<Coordinate> coordinates = new ArrayList<>();
        double angle = TWO_PI / npoints;
        double a = 0;
        while (a < TWO_PI) {
            double sx = (x + Math.cos(a) * rad);
            double sy = (y + Math.sin(a) * rad);
            a += angle;
            coordinates.add(new Coordinate(Math.floor(sx), Math.floor(sy)));
        }
        coordinates.add(coordinates.get(0));
        return coordinates.toArray(new Coordinate[0]);
    }
}
